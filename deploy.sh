#!/bin/bash 

# This script will deploy , 
# mattermost3.5.1, postgres9.6
# using oc new-app   

# creating a pvc
oc create -f storage0pv.yaml

# deploying postgresql
oc new-app -f postgresql.json

# creating required pvc 
oc create -f storage1pv.yaml 
oc create -f storage2pv.yaml

# deploying mattermost
oc new-app -f mattermost.json 

# creating a pvc to store backup
oc create -f storage3pv.yaml

# deploying recurring pgdump
oc new-app -f pgdump.json
